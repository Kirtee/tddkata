﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDD_OCT2016
{
    public class Adder
    {
        public int Add(string p1, string p2)
        {
            int n;

            if (p1 == null && p2 == null) return 0;

            if (p2 != null)
            {
                if (!int.TryParse(p2, out n))
                {
                    p2 = "0";
                }
                if (p1 == null)
                {
                    return int.Parse(p2);
                }
            }
            if (!int.TryParse(p1, out n))
            {
                p1 = "0";
            }
            if (p2 == null)
            {
                return int.Parse(p1);
            }

            return int.Parse(p1) + int.Parse(p2);
        }


        public int Add(string[] values)
        {
            int number = 0;

            return values.Where(value => value != null && int.TryParse(value, out number)).Sum(value => number);
        }
    }
}
