﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TDD_OCT2016
{
    public class StringCalculator
    {
        public int Add(string values)
        {
            int number = 0;
            int result = 0;
            uint num;
            List<string> negativeNumbers = new List<string>();

            char[] delimiters = new char[] {',', '\n', ' ', '/', ';', '*', '[', ']', '%', '&'};
            string[] splittedValues = values.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (var value in splittedValues.Where(value => !string.IsNullOrEmpty(value) && int.Parse(value) < 1000))
            {
                if (uint.TryParse(value, out num))
                {
                    result += int.Parse(value);
                }
                else
                {
                    negativeNumbers.Add(value);
                }
            }

            if (negativeNumbers.Count > 0)
            {
                StringBuilder msg = new StringBuilder();
                foreach (var str in negativeNumbers)
                {
                    msg.Append(str + " ");
                }

                throw new Exception("negatives not allowed: " + msg);
            }
            else
            {
                return result;
            }

        }
    }
}