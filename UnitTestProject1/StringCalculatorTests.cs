﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TDD_OCT2016;

namespace UnitTestProject1
{
    [TestClass]
    public class StringCalculatorTests
    {
        private StringCalculator _stringCalculator;

        [TestInitialize]
        public void Init()
        {
            _stringCalculator = new StringCalculator();
        }

       [TestMethod]
        public void Adding_Empty_number_gives_0()
        {
            var result = _stringCalculator.Add("");
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void Adding_Two_Empty_numbers_gives_0()
        {
            var result = _stringCalculator.Add(" , ");
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void Adding_One_number()
        {
            var result = _stringCalculator.Add("1");
            Assert.IsTrue(result == 1);
        }

        [TestMethod]
        public void Adding_1_plus_2_is_3()
        {
            var result = _stringCalculator.Add("1,2");
            Assert.IsTrue(result == 3);
        }

        [TestMethod]
        public void Adding_Numbers_With_Spaces()
        {
            var result = _stringCalculator.Add(" 1 , 2 ");
            Assert.IsTrue(result == 3);
        }

        [TestMethod]
        public void Adding_Numbers_With_Delimeters()
        {
            var result = _stringCalculator.Add("1\n2, 3");
            Assert.IsTrue(result == 6);
        }


        [TestMethod]
        public void Adding_Numbers_With_IncorrectDelimeters()
        {
            var result = _stringCalculator.Add("1,\n");
            Assert.IsTrue(result == 1);
        }

        [TestMethod]
        public void Adding_Numbers_With_DifferentDelimeters()
        {
            var result = _stringCalculator.Add("//;\n1;2");
            Assert.IsTrue(result == 3);
        }

        [TestMethod]
        public void Adding_NegativeNumbers()
        {
            var result = _stringCalculator.Add("-1, -2");
        }

        [TestMethod]
        public void Adding_One_NegativeNumber()
        {
            var result = _stringCalculator.Add("-1, abc");
        }

        [TestMethod]
        public void Adding_1000_And_2_Gives_2()
        {
            var result = _stringCalculator.Add("1001, 2");
            Assert.IsTrue(result == 2);
        }

        [TestMethod]
        public void Adding_Numbers_With_Multiple_Delimeters()
        {
            var result = _stringCalculator.Add("//[***]\n1***2***3");
            Assert.IsTrue(result == 6);
        }

        [TestMethod]
        public void Adding_Numbers_With_Multiple_Delimeters_AnyLength()
        {
            var result = _stringCalculator.Add("//[*][%]\n1*2%3");
            Assert.IsTrue(result == 6);
        }

        [TestMethod]
        public void Adding_Numbers_With_Multiple_Delimeters_With_Lenght_LongerThan_1Char()
        {
            var result = _stringCalculator.Add("//[***][%&]\n1***2%&3");
            Assert.IsTrue(result == 6);
        }
    }
}
