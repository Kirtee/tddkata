﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TDD_OCT2016;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private Adder _adder; 

        [TestInitialize]
        public void Init()
        {
            _adder = new Adder();
        }

        [TestMethod]
        public void Adding_2_plus_2_is_4()
        {
            var result = _adder.Add("2", "2");
            Assert.IsTrue(result == 4);
        }

        [TestMethod]
        public void Adding_Null_and_Null_Gives_Zero()
        {
            var result = _adder.Add(null, null);
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void Adding_NUll_and_1_gives_1()
        {
            var result = _adder.Add(null, "1");
            Assert.IsTrue(result == 1);
        }

        [TestMethod]
        public void Adding_1_and_Null_gives_1()
        {
            var result = _adder.Add("1", null);
            Assert.IsTrue(result == 1);
        }

        [TestMethod]
        public void If_p1_not_number_treat_as_zero()
        {
            var result = _adder.Add("abc", null);
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void If_p2_not_number_treat_as_zero()
        {
            var result = _adder.Add(null, "xyz");
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void If_not_number_treat_as_zero()
        {
            var result = _adder.Add("abc", "xyz");
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void Adding_3_3_3_gives_9()
        {
            string[] numbers = { "3", "3", "3" };

            var result = _adder.Add(numbers);
            Assert.IsTrue(result == 9);
        }
    }
}
